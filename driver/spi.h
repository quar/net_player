#ifndef _SPI_H_
#define _SPI_H_

#include "stm32f10x.h"
#include <stdint.h>

#define SPI_DMA

void spi_init(SPI_TypeDef* SPI);
uint8_t spi_send_byte(SPI_TypeDef* SPI, uint8_t byte);
uint8_t spi_recv_byte(SPI_TypeDef* SPI, uint8_t byte);
int spi_write(SPI_TypeDef* SPI, void *buffer, int size);
int spi_read(SPI_TypeDef* SPI, void *buffer, int size);
void spi_high_speed(SPI_TypeDef* SPI);
void spi_low_speed(SPI_TypeDef* SPI);
void spi_dma_recv_send(SPI_TypeDef* SPI, uint8_t* buf, uint16_t num);
void spi_dma_send(SPI_TypeDef* SPI, uint8_t* buf, uint16_t num);
void spi_dma_recv(SPI_TypeDef* SPI, uint8_t* buf, uint16_t num);

#endif

