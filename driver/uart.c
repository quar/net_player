#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include "uart.h"
#include "config.h"
#include "stm32f10x.h"
#include "rthw.h"


static char uart_buffer[UART_LEN];
#ifdef  USE_OS
static rt_mutex_t g_prt_log_mutex;
#endif

uint8_t uart_init(void)
{
    USART_InitTypeDef USART_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 , ENABLE);

    USART_InitStructure.USART_BaudRate = 256000;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No ;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_Init(USART1, &USART_InitStructure);
    USART_Cmd(USART1, ENABLE);

#ifdef USE_OS
    g_prt_log_mutex = rt_mutex_create("log mutex", RT_IPC_FLAG_PRIO);
    if (g_prt_log_mutex == NULL) {
        return ERR_NOMEM;
    }
#endif

    return ERR_OK;
}
#if 0
void UsartSendData(USART_TypeDef* USARTx,u8 *dta, u32 len)
{
    u32 pos = 0;
    while(USART_GetFlagStatus(USARTx, USART_FLAG_TC)== RESET);
    while(size--)
    {
        UsartSendByte(USARTx,*(data+pos));
        pos++;
    }
}

static void UsartSendByte(USART_TypeDef* USARTx,u8 ch)
{
    USART_SendData(USARTx, (u8)ch);

    while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE)== RESET);
}

static void UsartSendString(USART_TypeDef* USARTx,u8 *str)
{
    u32 pos = 0;
    while(USART_GetFlagStatus(USARTx, USART_FLAG_TC)== RESET);
    while(*(str+pos)!='\0')
    {
        UsartSendByte(USARTx,*(str+pos));
        pos++;
    }
}

void LOG(const char *fmt, ...)
{
    va_list args;
    short len;
    
    rt_mutex_take(g_prt_log_mutex,RT_WAITING_FOREVER);
    va_start(args,fmt);
    len = vsnprintf((char *)uart_buffer,UART_LEN - 1,fmt,args);
    va_end(args);
    UsartSendString(USART1, (uint8_t *)uart_buffer);
    rt_mutex_release(g_prt_log_mutex);
}
#else
int fputc(int ch, FILE *f)
{
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
    USART_SendData(USART1, (unsigned char) ch);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    return (ch);
}

void USART1_Putc(unsigned char c)
{
    USART_SendData(USART1, c);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}

void USART1_Puts(char * str)
{
    while(*str)
    {
        USART_SendData(USART1, *str++);
        while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    }
}
#endif
