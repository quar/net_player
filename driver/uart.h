#ifndef _UART_H_
#define _UART_H_
#include <stdio.h>
#include "stm32f10x.h"
//#define DEBUG_MCU
#define UART_LEN 120

//__attribute__(( format (archetype, string-index, first-to-check)))
void LOG(const char *fmt, ...) __attribute__((format(printf,1,2)));

//#ifdef DEBUG_MCU 
//#define PRINT(cmd,fmt,...) //LOG("+"cmd":"fmt,##__VA_ARGS__)
//#endif



#define LOG_ERROR                      (0x01)
#define LOG_WARNING                    (0x02)
#define LOG_NOTICE                     (0x04)
#define LOG_INFO                       (0x08)
#define LOG_DEBUG                      (0x10)
 
/* This log macro must be set befor compiler, output error, warning, info debug lever */
#define DEBUG_LEVEL_SWITCH             (0xFF)
#define LOGHEARD                       "LOGINFO"
#define DebugPrintf(fmt, arg...)          LOG((const char*)fmt, ##arg) 
 
#if ((DEBUG_LEVEL_SWITCH & LOG_ERROR) == LOG_ERROR)
    #define log_err(fmt, arg...)      DebugPrintf("%s:[%s@%s,%d]"fmt"\r\n",LOGHEARD,__FUNCTION__,__FILE__,__LINE__, ##arg)
#else
    #define log_err(fmt, arg...)
#endif
 
#if ((DEBUG_LEVEL_SWITCH & LOG_WARNING) == LOG_WARNING)
    #define log_warn(fmt, arg...)      DebugPrintf("%s:"fmt"\r\n",LOGHEARD, ##arg)
#else
    #define log_warn(fmt, arg...)
#endif
 
#if ((DEBUG_LEVEL_SWITCH & LOG_NOTICE) == LOG_NOTICE)
    #define log_notice(fmt, arg...)      DebugPrintf("%s:"fmt"\r\n",LOGHEARD, ##arg)
#else
    #define log_notice(fmt, arg...)
#endif
 
#if ((DEBUG_LEVEL_SWITCH & LOG_INFO) == LOG_INFO)
    #define log_info(fmt, arg...)      DebugPrintf("%s:"fmt"\r\n",LOGHEARD, ##arg)
#else
    #define log_info(fmt, arg...)
#endif
 
#if ((DEBUG_LEVEL_SWITCH & LOG_DEBUG) == LOG_DEBUG)
#define log_debug(fmt, arg...)      DebugPrintf("%s:[%s@%s,%d]"fmt"\r\n",LOGHEARD,__FUNCTION__,__FILE__,__LINE__, ##arg)
#else
    #define log_debug(fmt, arg...)
#endif
 
#if ((DEBUG_LEVEL_SWITCH & LOG_DEBUG) == LOG_DEBUG)
void UsartSendData(USART_TypeDef* USARTx,u8 *dta, u32 len);
#define log_data(data,size)    UsartSendData(USART1,data,size)
#else
    #define log_data(data, size)
#endif


uint8_t uart_init(void);
int fputc(int ch, FILE *f);
void USART1_Putc(unsigned char c);
void USART1_Puts(char * str);
void USART1_IRQHandler(void);

#endif
