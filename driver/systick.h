#ifndef _SYSTICK_H_
#define _SYSTICK_H_
#include <stdint.h>
#include "stm32f10x.h"

void Systick_Init (u8 SYSCLK);
void Delay_ms( uint32_t time_ms );
void Delay_us( uint32_t time_us );

#endif