#ifndef _VS1053B_H_
#define _VS1053B_H_
#include <stdint.h>

#define VS1053B_SPI                 SPI3
#define VS1053B_XDCS_PORT           GPIOC
#define VS1053B_XDCS_PIN            GPIO_Pin_4
#define VS1053B_XCS_PORT            GPIOA
#define VS1053B_XCS_PIN             GPIO_Pin_2
#define VS1053B_DREQ_PORT           GPIOE
#define VS1053B_DREQ_PIN            GPIO_Pin_4
#define VS1053B_RST_PORT            GPIOE
#define VS1053B_RST_PIN             GPIO_Pin_3

#define VS10xx_VS_WRITE_COMMAND     0x02
#define VS10xx_VS_READ_COMMAND      0x03

#define VS10xx_REG_MODE             0x00
#define VS10xx_REG_STATUS           0x01
#define VS10xx_REG_BASS             0x02
#define VS10xx_REG_CLOCKF           0x03
#define VS10xx_REG_DECODE_TIME      0x04
#define VS10xx_REG_AUDATA           0x05
#define VS10xx_REG_WRAM             0x06
#define VS10xx_REG_WRAMADDR         0x07
#define VS10xx_REG_HDAT0            0x08
#define VS10xx_REG_HDAT1            0x09
#define VS10xx_REG_AIADDR           0x0a
#define VS10xx_REG_VOL              0x0b
#define VS10xx_REG_AICTRL0          0x0c
#define VS10xx_REG_AICTRL1          0x0d
#define VS10xx_REG_AICTRL2          0x0e
#define VS10xx_REG_AICTRL3          0x0f

#define VS10xx_SM_DIFF              0x01
#define VS10xx_SM_JUMP              0x02
#define VS10xx_SM_RESET             0x04
#define VS10xx_SM_OUTOFWAV          0x08
#define VS10xx_SM_PDOWN             0x10
#define VS10xx_SM_TESTS             0x20
#define VS10xx_SM_STREAM            0x40
#define VS10xx_SM_PLUSV             0x80
#define VS10xx_SM_DACT              0x100
#define VS10xx_SM_SDIORD            0x200
#define VS10xx_SM_SDISHARE          0x400
#define VS10xx_SM_SDINEW            0x800
#define VS10xx_SM_ADPCM             0x1000
#define VS10xx_SM_ADPCM_HP          0x2000

#define I2S_CONFIG                  0XC040
#define GPIO_DDR                    0XC017
#define GPIO_IDATA                  0XC018
#define GPIO_ODATA                  0XC019

#define VS10xx_DEFAULT_VOLUME       220

uint8_t vs10xx_write(uint8_t data[32], uint8_t w_len);
void vs10xx_init(void);
void vs10xx_sin_test(void);
uint16_t vs10xx_ram_test(void);
void vs10xx_restart_play(void);
void vs10xx_set_volume(uint8_t vol);

#endif