#ifndef _W5500_PORT_
#define _W5500_PORT_

#include "wizchip_conf.h"
#include "stm32f10x.h"

#define W5500_SPI           SPI2
#define W5500_CS_PORT       GPIOB
#define W5500_CS_PIN        GPIO_Pin_12
#define W5500_RST_PORT      GPIOC
#define W5500_RST_PIN       GPIO_Pin_7
#define W5500_INT_PORT      GPIOC
#define W5500_INT_PIN       GPIO_Pin_6


#define DEFAULT_MAC_ADDR    {0x00,0xf1,0xbe,0xc4,0xa1,0x05}
#define DEFAULT_IP_ADDR     {192,168,0,136}
#define DEFAULT_SUB_MASK    {255,255,255,0}
#define DEFAULT_GW_ADDR     {192,168,0,1}
#define DEFAULT_DNS_ADDR    {8,8,8,8}
#define DEST_IP_ADDR        {192,168,0,140}
#define LOCAL_SOCKET_NUM    1
#define LOCAL_SOCKET_PORT   12345
#define DEST_SOCKET_NUM     1
#define DEST_SOCKET_PORT    12345


/* 定义该宏则表示使用自动协商模式，取消则设置为100M全双工模式 */
//#define USE_AUTONEGO

/* 定义该宏则表示在初始化网络信息时设置DHCP */
//#define USE_DHCP

//#pragma pack(1)

typedef struct _udps_addr_info {
    uint8_t  ip[4];
    uint16_t port;
    uint8_t sn;
} udps_addr_info_t;

//#pragma pack()

void w5500_network_info_show(void);
int w5500_udps_send(udps_addr_info_t *dest, uint8_t *buf, uint16_t len);
uint16_t w5500_udps_recv(udps_addr_info_t *dest, uint8_t *buf, uint32_t len);
int w5500_init(udps_addr_info_t *local_conf);


#endif
