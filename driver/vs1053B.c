#include "vs1053B.h"
#include "spi.h"
#include "stdint.h"
#include "systick.h"
#include "stm32f10x.h"

/* CLK的值 12.28 *3  约等于 36M. (外部时钟是12.28M,内部时钟频率为输入时钟频率的3倍,所以clk = 12 * 3)
* SCI 读取的时候，SPI 不能超过clk/7=5M.
* SCI 和 SDI 写入的时候，SPI 不能超过clk/4=9M.
*/
 
static void vs10xx_write_byte(uint8_t data)
{
    spi_send_byte(VS1053B_SPI, data);
}

static uint8_t vs10xx_read_byte(void)
{
    return spi_recv_byte(VS1053B_SPI, 0);
}

static void vs10xx_write_register(uint8_t addr,uint16_t data)
{
    while (GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN) == 0);

    spi_low_speed(VS1053B_SPI);
    GPIO_SetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);
    GPIO_ResetBits(VS1053B_XCS_PORT, VS1053B_XCS_PIN);
    vs10xx_write_byte(VS10xx_VS_WRITE_COMMAND);
    vs10xx_write_byte(addr);
    vs10xx_write_byte(data>>8);
    vs10xx_write_byte(data);
    GPIO_SetBits(VS1053B_XCS_PORT, VS1053B_XCS_PIN);
    spi_high_speed(VS1053B_SPI);
}

static uint16_t vs10xx_read_register(uint8_t addr)
{
    uint16_t reg;

    while (GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN) == 0);

    spi_low_speed(VS1053B_SPI);
    GPIO_SetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);
    GPIO_ResetBits(VS1053B_XCS_PORT, VS1053B_XCS_PIN);
    vs10xx_write_byte(VS10xx_VS_READ_COMMAND);
    vs10xx_write_byte(addr);
    reg = vs10xx_read_byte();
    reg = reg<<8;
    reg += vs10xx_read_byte();
    GPIO_SetBits(VS1053B_XCS_PORT, VS1053B_XCS_PIN);
    spi_high_speed(VS1053B_SPI);

    return reg;
}

static void vs10xx_write_ram(uint16_t addr,uint16_t val)
{
    uint8_t retry = 0xff;

    while (GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN) == 0);

    vs10xx_write_register(VS10xx_REG_WRAMADDR, addr);
    while (GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN) == 0);
    vs10xx_write_register(VS10xx_REG_WRAM, val);
}

static uint16_t vs10xx_read_ram(uint16_t addr)
{
    vs10xx_write_register(VS10xx_REG_WRAMADDR, addr);
    return vs10xx_read_register(VS10xx_REG_WRAM);
}

uint8_t vs10xx_write(uint8_t data[32], uint8_t w_len)
{
    uint8_t i;

    if (GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN) == 0) {
        return 1;
    }
    __disable_irq();
    spi_high_speed(VS1053B_SPI);
    GPIO_ResetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);

    for (i = 0; i < w_len; i++) {
        vs10xx_write_byte(data[i]);
    }
    GPIO_SetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);
    __enable_irq();

    return 0;
}

//设定VS10XX播放的音量和高低音
//volx:音量大小(0~254)
void vs10xx_set_volume(uint8_t vol)
{
    uint16_t volt=0;

    volt=254-vol;
    volt<<=8;
    volt|=(254-vol);
    vs10xx_write_register(VS10xx_REG_VOL, volt);

    return;
}

//设置播放速度（仅VS1053有效）
//t:0,1,正常速度;2,2倍速度;3,3倍速度;4,4倍速;以此类推
void vs10xx_set_speed(u8 t)
{
    vs10xx_write_ram(0X1E04,t);  //写入播放速度
}

//设定高低音控制
//bfreq:低频上限频率          2~15(单位:10Hz)
//bass:低频增益         0~15(单位:1dB)
//tfreq:高频下限频率 1~15(单位:Khz)
//treble:高频增益       0~15(单位:1.5dB,小于9的时候为负数)
void vs10xx_set_bass(uint8_t bfreq,uint8_t bass,uint8_t tfreq,uint8_t treble)
{
    uint16_t bass_set=0;
    uint8_t temp=0;

    if(treble==0)temp=0;
    else if(treble>8)temp=treble-8;
    else temp=treble-9;
    bass_set=temp&0X0F;
    bass_set<<=4;
    bass_set+=tfreq&0xf;
    bass_set<<=4;
    bass_set+=bass&0xf;
    bass_set<<=4;
    bass_set+=bfreq&0xf;

    vs10xx_write_register(VS10xx_REG_BASS,bass_set);
}

//设定音效
//eft:0,关闭;1,最小;2,中等;3,最大.
void vs10xx_set_effect(uint8_t eft)
{
    uint16_t temp;

    temp=vs10xx_read_register(VS10xx_REG_MODE);
    if(eft&0X01)temp|=1<<4;
    else temp&=~(1<<5);
    if(eft&0X02)temp|=1<<7;
    else temp&=~(1<<7);

    vs10xx_write_register(VS10xx_REG_MODE,temp);
}

void vs10xx_set_default_vol(void)
{
    vs10xx_set_volume(VS10xx_DEFAULT_VOLUME);
}

void vs10xx_soft_reset(void)
{
    uint8_t retry=0;

    while(GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN)==0);        //等待软件复位结束
    vs10xx_write_byte(0Xff);         //启动传输
    retry=0;
    while(vs10xx_read_register(VS10xx_REG_MODE)!=0x0800)  // 软件复位,新模式
    {
        vs10xx_write_register(VS10xx_REG_MODE,0x0804);     // 软件复位,新模式
        Delay_ms(2);//等待至少1.35ms 
        if(retry++>100)break;
    }            
    while(GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN)==0);//等待软件复位结束
    retry=0;
    while(vs10xx_read_register(VS10xx_REG_CLOCKF)!=0X9800)//设置VS1053的时钟,3倍频 ,1.5xADD
    {
        vs10xx_write_register(VS10xx_REG_CLOCKF,0X9800);   //设置VS1053的时钟,3倍频 ,1.5xADD
        if(retry++>100)break;
    }
    Delay_ms(20);
}

uint8_t vs10xx_hard_reset()
{
    uint8_t retry=0;

    GPIO_ResetBits(VS1053B_RST_PORT, VS1053B_RST_PIN);
    Delay_ms(20);
    GPIO_SetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);//取消数据传输
    GPIO_SetBits(VS1053B_XCS_PORT, VS1053B_XCS_PIN);//取消数据传输
    GPIO_SetBits(VS1053B_RST_PORT, VS1053B_RST_PIN);
    while(GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN) == 0 && retry < 200)//等待DREQ为高
    {
        retry++;
        Delay_us(50);
    }
    Delay_ms(20);
    if(retry>=200)return 1;
    else return 0;
}

void vs10xx_sin_test(void)
{
    vs10xx_hard_reset();
    vs10xx_set_default_vol();
    //vs10xx_write_register(VS10xx_REG_VOL,0X2020);
    vs10xx_write_register(VS10xx_REG_MODE,0x0820);
    while(GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN) == 0);
    spi_low_speed(VS1053B_SPI);
    GPIO_ResetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);
    vs10xx_write_byte(0x53);
    vs10xx_write_byte(0xef);
    vs10xx_write_byte(0x6e);
    vs10xx_write_byte(0x24);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    Delay_ms(100);
    GPIO_SetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);
    //退出正弦测试
     GPIO_ResetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);//选中数据传输
    vs10xx_write_byte(0x45);
    vs10xx_write_byte(0x78);
    vs10xx_write_byte(0x69);
    vs10xx_write_byte(0x74);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    Delay_ms(100);
    GPIO_SetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);
}

uint16_t vs10xx_ram_test(void)
{
    vs10xx_hard_reset();
    vs10xx_write_register(VS10xx_REG_MODE,0x0820);// 进入VS10XX的测试模式
    while(GPIO_ReadInputDataBit(VS1053B_DREQ_PORT, VS1053B_DREQ_PIN) == 0); // 等待DREQ为高
    spi_low_speed(VS1053B_SPI);//低速
    GPIO_ResetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);     // xDCS = 0，选择VS10XX的数据接口
    vs10xx_write_byte(0x4d);
    vs10xx_write_byte(0xea);
    vs10xx_write_byte(0x6d);
    vs10xx_write_byte(0x54);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    vs10xx_write_byte(0x00);
    Delay_ms(150);
    GPIO_SetBits(VS1053B_XDCS_PORT, VS1053B_XDCS_PIN);
    return vs10xx_read_register(VS10xx_REG_HDAT0);// VS1053如果得到的值为0x83FF，则表明完好;
}

//得到平均字节数
//返回值：平均字节数速度
uint16_t vs10xx_get_byte_rate(void)
{
    return vs10xx_read_ram(0X1E05);//平均位速
}

//得到需要填充的数字
//返回值:需要填充的数字
uint16_t vs10xx_get_fill_byte(void)
{
    return vs10xx_read_ram(0X1E06);//填充字节
} 

//切歌
//通过此函数切歌，不会出现切换“噪声”
void vs10xx_restart_play(void)
{
    uint16_t temp;
    uint16_t i;
    uint8_t n;
    uint8_t vsbuf[32] = {0};

    temp=vs10xx_read_register(VS10xx_REG_MODE);   //读取SPI_MODE的内容
    temp|=1<<3;                 //设置SM_CANCEL位
    temp|=1<<2;                 //设置SM_LAYER12位,允许播放MP1,MP2
    vs10xx_write_register(VS10xx_REG_MODE,temp);   //设置取消当前解码指令
    for(i=0;i<2048;)                //发送2048个0,期间读取SM_CANCEL位.如果为0,则表示已经取消了当前解码
    {
        if(vs10xx_write(vsbuf,32)==0)//每发送32个字节后检测一次
        {
            i+=32;                      //发送了32个字节
            temp=vs10xx_read_register(VS10xx_REG_MODE);   //读取SPI_MODE的内容
            if((temp&(1<<3))==0)break;  //成功取消了
        }
    }
    if(i<2048)//SM_CANCEL正常
    {
        temp=vs10xx_get_fill_byte()&0xff;//读取填充字节
        for(n=0;n<32;n++)vsbuf[n]=temp;//填充字节放入数组
        for(i=0;i<2052;)
        {
            if(vs10xx_write(vsbuf,32)==0)i+=32;//填充
        }
    }else vs10xx_soft_reset();      //SM_CANCEL不成功,坏情况,需要软复位
    temp = vs10xx_read_register(VS10xx_REG_HDAT0);
    temp += vs10xx_read_register(VS10xx_REG_HDAT1);
    if(temp)            //软复位,还是没有成功取消,放杀手锏,硬复位
    {
        vs10xx_hard_reset();        //硬复位
        vs10xx_soft_reset();        //软复位
    } 
}

//重设解码时间
void vs10xx_reset_decode_time(void)
{
    vs10xx_write_register(VS10xx_REG_DECODE_TIME,0x0000);
    vs10xx_write_register(VS10xx_REG_DECODE_TIME,0x0000);//操作两次
}
//得到mp3的播放时间n sec
//返回值：解码时长
uint16_t vs10xx_get_decode_time(void)
{
    uint16_t dt=0;

    dt = vs10xx_read_register(VS10xx_REG_DECODE_TIME);

    return dt;
}

void vs10xx_init(void)
{
    vs10xx_hard_reset();        //硬复位
    vs10xx_soft_reset();        //软复位
    vs10xx_set_default_vol();
}
