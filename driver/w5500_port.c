#include <stdio.h>
#include <string.h>
#include "w5500_port.h"
#include "spi.h"
#include "systick.h"
#include "socket.h"
#include "uart.h"

/**
 * @brief   enter critical section
 * @param   none
 * @return  none
 */
static void w5500_cris_enter(void)
{
    __set_PRIMASK(1);
}

/**
 * @brief   exit critical section
 * @param   none
 * @return  none
 */
static void w5500_cris_exit(void)
{
    __set_PRIMASK(0);
}

/**
 * @brief   select chip
 * @param   none
 * @return  none
 */
static void w5500_cs_select(void)
{
    GPIO_ResetBits(W5500_CS_PORT, W5500_CS_PIN);
}

/**
 * @brief   deselect chip
 * @param   none
 * @return  none
 */
static void w5500_cs_deselect(void)
{
    GPIO_SetBits(W5500_CS_PORT, W5500_CS_PIN);
}

/**
 * @brief   read byte in SPI interface
 * @param   none
 * @return  the value of the byte read
 */
static uint8_t w5500_spi_readbyte(void)
{
    #ifdef  SPI_DMA
    uint8_t rb;
    spi_dma_recv(W5500_SPI, &rb, 1);
    
    return rb;
    #else
    return spi_recv_byte(W5500_SPI, 0);
    #endif
}

/**
 * @brief   write byte in SPI interface
 * @param   wb  the value to write
 * @return  none
 */
static void w5500_spi_writebyte(uint8_t wb)
{
    #ifdef  SPI_DMA
    spi_dma_send(W5500_SPI, &wb, 1);
    #else
    spi_send_byte(W5500_SPI, wb);
    #endif
}

/**
 * @brief   burst read byte in SPI interface
 * @param   pBuf    pointer of data buf
 * @param   len     number of bytes to read
 * @return  none
 */
static void w5500_spi_readburst(uint8_t* pBuf, uint16_t len)
{
    if (!pBuf) {
        return;
    }
    #ifdef  SPI_DMA
    spi_dma_recv(W5500_SPI, pBuf, len);
    #else
    spi_read(W5500_SPI, pBuf, len);
    #endif
}

/**
 * @brief   burst write byte in SPI interface
 * @param   pBuf    pointer of data buf
 * @param   len     number of bytes to write
 * @return  none
 */
static void w5500_spi_writeburst(uint8_t* pBuf, uint16_t len)
{
    if (!pBuf) {
        return;
    }
    #ifdef  SPI_DMA
    spi_dma_send(W5500_SPI, pBuf, len);
    #else
    spi_write(W5500_SPI, pBuf, len);
    #endif
}

/**
 * @brief   hard reset
 * @param   none
 * @return  none
 */
static void w5500_hard_reset(void)
{
    GPIO_ResetBits(W5500_RST_PORT, W5500_RST_PIN);
    Delay_us(500);
    GPIO_SetBits(W5500_RST_PORT, W5500_RST_PIN);
    Delay_ms(100);
}

/**
 * @brief   Initializes WIZCHIP with socket buffer size
 * @param   none
 * @return  errcode
 * @retval  0   success
 * @retval  -1  fail
 */
static int w5500_chip_init(void)
{
    /* default size is 2KB */
    uint8_t rx_size[8] = {4,4,4,4,0,0,0,0};       //改RX的 socket0 - 4 容量从默认2K到4K
    return wizchip_init(NULL, NULL);
}

/**
 * @brief   set phy config if autonego is disable
 * @param   none
 * @return  none
 */
static void w5500_phy_init(void)
{
#ifdef USE_AUTONEGO
    // no thing to do
#else
    wiz_PhyConf conf;
    
    conf.by = PHY_CONFBY_SW;
    conf.mode = PHY_MODE_MANUAL;
    conf.speed = PHY_SPEED_100;
    conf.duplex = PHY_DUPLEX_FULL;
    
    wizphy_setphyconf(&conf);
#endif
}

/**
 * @brief   initializes the network infomation
 * @param   none
 * @return  none
 */
static void w5500_network_info_init(void)
{
    wiz_NetInfo info;
    
    uint8_t mac[6] = DEFAULT_MAC_ADDR;
    uint8_t ip[4] = DEFAULT_IP_ADDR;
    uint8_t sn[4] = DEFAULT_SUB_MASK;
    uint8_t gw[4] = DEFAULT_GW_ADDR;
    uint8_t dns[4] = DEFAULT_DNS_ADDR;
    
    memcpy(info.mac, mac, 6);
    memcpy(info.ip, ip, 4);
    memcpy(info.sn, sn, 4);
    memcpy(info.gw, gw, 4);
    memcpy(info.dns, dns, 4);
    
#ifdef USE_DHCP
    info.dhcp = NETINFO_DHCP;
#else
    info.dhcp = NETINFO_STATIC;
#endif
    
    wizchip_setnetinfo(&info);
}

/**
 * @brief   read and show the network infomation
 * @param   none
 * @return  none
 */
void w5500_network_info_show(void)
{
    wiz_NetInfo info = {0};
    
    wizchip_getnetinfo(&info);
    
    printf("w5500 network infomation:\r\n");
    printf("  -mac:%d:%d:%d:%d:%d:%d\r\n", info.mac[0], info.mac[1], info.mac[2], 
            info.mac[3], info.mac[4], info.mac[5]);
    printf("  -ip:%d.%d.%d.%d\r\n", info.ip[0], info.ip[1], info.ip[2], info.ip[3]);
    printf("  -sn:%d.%d.%d.%d\r\n", info.sn[0], info.sn[1], info.sn[2], info.sn[3]);
    printf("  -gw:%d.%d.%d.%d\r\n", info.gw[0], info.gw[1], info.gw[2], info.gw[3]);
    printf("  -dns:%d.%d.%d.%d\r\n", info.dns[0], info.dns[1], info.dns[2], info.dns[3]);
    
    if (info.dhcp == NETINFO_DHCP) {
        printf("  -dhcp_mode: dhcp\r\n");
    } else {
        printf("  -dhcp_mode: static\r\n");
    }
}

/**
 * @brief   w5500 udps init
 * @param   local    socket number and listen port
 * @retval  -1       init fail
            0 ~ 7    socket number
 */
static int w5500_udps_init(udps_addr_info_t *local)
{
    if (local == NULL) {
        return -1;
    }
    
    if (local->sn > 8) {
        return -1;
    }
    if(SOCK_CLOSED != getSn_SR(local->sn)) {
        return -1;
    }
    
    return  socket(local->sn, Sn_MR_UDP, local->port, 0x00);
}

/**
 * @brief   w5500 udps send data
 * @param   dest   dest info
 * @param   buf    point to send data
 * @param   len    the length of send data
 * @retval  >0     send ok
            <0     send failed
 */
int w5500_udps_send(udps_addr_info_t *dest, uint8_t *buf, uint16_t len)
{
    int ret = 0;
    uint16_t sendsize = 0;

    if(SOCK_UDP == getSn_SR(dest->sn)) {
        while(sendsize != len)
        {
           ret = sendto(dest->sn, buf + sendsize, len - sendsize, dest->ip, dest->port);
           if(ret < 0)
           {
              printf("%d: sendto error. %d\r\n",dest->sn,ret);
              return ret;
           }
           sendsize += ret; // Don't care SOCKERR_BUSY, because it is zero.
        }
    }

    return ret;
}

/**
 * @brief   w5500 udps recevie data
 * @param   dest   dest info
 * @param   buf    point to recv data
 * @param   len    the max length of the buf
 * @retval  size   recevie len
 */
uint16_t w5500_udps_recv(udps_addr_info_t *dest, uint8_t *buf, uint32_t len)
{
    int32_t  ret;
    uint16_t size = 0;

    if(SOCK_UDP == getSn_SR(dest->sn)) {
         if((size = getSn_RX_RSR(dest->sn)) > 0)
         {
            if(size > 2048) size = 2048;
            ret = recvfrom(dest->sn, buf, size, dest->ip, (uint16_t*)&dest->port);
            printf("size %d,ret %d\r\n",size, ret);
            if (ret > len) {
                printf("w5500 recv error. size=%d,len=%d\r\n",size, len);
                return 0;
            }
            if(ret <= 0)
            {
#ifdef _LOOPBACK_DEBUG_
               printf("%d: recvfrom error. %ld\r\n",sn,ret);
#endif
               return ret;
            }
            size = (uint16_t) ret;
         }
    }

    return size;
}

/**
 * @brief   w5500 init
 * @param   none
 * @return  errcode
 * @retval  0   success
 * @retval  -1  chip init fail
 */
int w5500_init(udps_addr_info_t *local_conf)
{
    if (local_conf == NULL) {
        return -1;
    }

    /* W5500 hard reset */
    w5500_hard_reset();
    
    /* Register spi driver function */
    reg_wizchip_cris_cbfunc(w5500_cris_enter, w5500_cris_exit);
    reg_wizchip_cs_cbfunc(w5500_cs_select, w5500_cs_deselect);
    reg_wizchip_spi_cbfunc(w5500_spi_readbyte, w5500_spi_writebyte);
    reg_wizchip_spiburst_cbfunc(w5500_spi_readburst, w5500_spi_writeburst);

    /* socket buffer size init */
    if (w5500_chip_init() != 0) {
        return -1;
    }

    /* phy init */
    w5500_phy_init();

    /* network infomation init */
    w5500_network_info_init();

    /* show network infomation */
    w5500_network_info_show();

    /*udp protocol init*/
    w5500_udps_init(local_conf);
    
    printf("mssr:%d\r\n",getSn_MSSR(local_conf->sn));
    return 0;
}
