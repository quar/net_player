#include "stm32f10x.h"
#include "config.h"

static u8  fac_us = 0;
static u16 fac_ms = 0;

#ifdef USE_OS
#include "rthw.h"
#include "rtthread.h"


void rt_hw_us_delay(rt_uint32_t us)
{
    rt_uint32_t ticks;
    rt_uint32_t told, tnow, tcnt = 0;
    rt_uint32_t reload = SysTick->LOAD;

    /* 获得延时经过的 tick 数 */
    ticks = us * reload / (1000000 / RT_TICK_PER_SECOND);
    /* 获得当前时间 */
    told = SysTick->VAL;
    while (1)
    {
        /* 循环获得当前时间，直到达到指定的时间后退出循环 */
        tnow = SysTick->VAL;
        if (tnow != told)
        {
            if (tnow < told)
            {
                tcnt += told - tnow;
            }
            else
            {
                tcnt += reload - tnow + told;
            }
            told = tnow;
            if (tcnt >= ticks)
            {
                break;
            }
        }
    }
}
#endif

void Systick_Init (u8 SYSCLK)
{
    SysTick->CTRL &= 0xfffffffb;
    fac_us = SYSCLK/8;
    fac_ms = (u16)fac_us*1000;
}

#ifdef USE_OS
void Delay_ms( uint32_t time_ms )
{
    rt_thread_mdelay(time_ms);
}
#else
void Delay_ms( uint32_t time_ms )
{
    u32 temp;
    SysTick->LOAD = (uint32_t)time_ms * fac_ms;//时间加载(SysTick->LOAD为24位)
    SysTick->VAL = 0x00;                               //清空计数器
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk ;          //开始倒数
    do
    {
    	temp = SysTick->CTRL;
    }
    while(temp&0x01&&!(temp&(1<<16)));
    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    SysTick->VAL = 0x00;
}
#endif

#ifdef USE_OS
void Delay_us( uint32_t time_us )
{
    rt_hw_us_delay(time_us);
}
#else
void Delay_us( uint32_t time_us )
{
    uint32_t temp;
    SysTick->LOAD = time_us * fac_us;                 //时间加载
    SysTick->VAL = 0x00;                            //清空计数器
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk ;      //开始倒数
    do
    {
        temp = SysTick->CTRL;
    }
    while(temp&0x01&&!(temp&(1<<16)));            //等待时间到达
    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;      //关闭计时器
    SysTick->VAL = 0X00;                           //清空计数器
}

#endif