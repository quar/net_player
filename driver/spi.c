#include "stm32f10x.h"
#include "stdint.h"
#include "spi.h"


void spi_init(SPI_TypeDef* SPI)
{
    SPI_InitTypeDef   SPI_InitStructure;

    if (SPI == SPI3) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);
    } else if (SPI == SPI2) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
    }

    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    if (SPI == SPI3) {
        SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
        SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
        SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
    } else if (SPI == SPI2) {
        SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
        SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
        SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
    }
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;

    SPI_Init(SPI, &SPI_InitStructure);
    SPI_Cmd(SPI, ENABLE);
    #ifdef  SPI_DMA
    SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Tx | SPI_I2S_DMAReq_Rx, ENABLE);
    #endif
}

void spi_dma_send(SPI_TypeDef* SPI, uint8_t* buf, uint16_t num)
{
    while((SPI->SR & SPI_I2S_FLAG_TXE ) == RESET);
    DMA1->IFCR |= DMA1_FLAG_TC5;                 //发送前清除ISR标志位
    DMA1_Channel5->CCR &= ~DMA_CCR5_EN;          //关闭DMA通道5
    DMA1_Channel5->CMAR = (u32)buf;              //设置DMA存储器地址
    DMA1_Channel5->CNDTR = num;                  //传输数量设置
    DMA1_Channel5->CCR |= DMA_CCR5_EN;           //开启DMA通道5
    while((DMA1->ISR & DMA1_FLAG_TC5 ) == RESET);
    DMA1->IFCR |= DMA1_FLAG_TC5;
    while((SPI->SR & SPI_I2S_FLAG_TXE ) == RESET);
    DMA1_Channel5->CCR &= ~DMA_CCR5_EN;         //关闭DMA通道5
}

void spi_dma_recv(SPI_TypeDef* SPI, uint8_t* buf, uint16_t num)
{
    SPI->CR1 |= SPI_Direction_2Lines_RxOnly;
    buf[0] = SPI->DR;                                  //接送前读一次SPI1->DR，保证接收缓冲区为空
    DMA1->IFCR |= DMA1_FLAG_TC4;                       //接收前清除ISR标志位
    DMA1_Channel4->CCR &= ~DMA_CCR4_EN;                //关闭DMA通道4
    DMA1_Channel4->CMAR = (u32)buf;                    //设置DMA存储器地址
    DMA1_Channel4->CNDTR = num;                        //传输数量设置
    DMA1_Channel4->CCR |= DMA_CCR4_EN;                 //开启DMA通道4
    while((DMA1->ISR & DMA1_FLAG_TC4 ) == RESET);
    DMA1->IFCR |= DMA1_FLAG_TC4;
    DMA1_Channel4->CCR &= ~DMA_CCR4_EN;                //关闭DMA通道4
    SPI->CR1 &= ~SPI_Direction_2Lines_RxOnly;
}

void spi_dma_recv_send(SPI_TypeDef* SPI, uint8_t* buf, uint16_t num)
{
    DMA1_Channel4->CCR &= ~DMA_CCR4_EN;         //关闭DMA通道4
    DMA1_Channel5->CCR &= ~DMA_CCR5_EN;         //关闭DMA通道5
    DMA1_Channel4->CPAR = (uint32_t)(&SPI->DR);      //设置外设地址
    DMA1_Channel4->CMAR = (u32)buf;           //设置DMA存储器地址
    DMA1_Channel5->CPAR = (uint32_t)(&SPI->DR);      //设置外设地址
    DMA1_Channel5->CMAR = (u32)buf;           //设置DMA存储器地址
    DMA1_Channel4->CNDTR = num;              //传输数量设置为buffersize个
    DMA1_Channel5->CNDTR = num;              //传输数量设置为buffersize个
    DMA1->IFCR &= ~(DMA1_FLAG_TC4 | DMA1_FLAG_TC5);                      //清除通道4,5的中断标志位
    SPI->DR;                                //接送前读一次SPI1->DR，保证接收缓冲区为空
    while((SPI->SR & SPI_I2S_FLAG_RXNE ) == RESET);
    DMA1_Channel4->CCR |= DMA_CCR4_EN;              //开启DMA通道4
    DMA1_Channel5->CCR |= DMA_CCR5_EN;              //开启DMA通道5
    while((DMA1->ISR & DMA1_FLAG_TC5 ) != DMA1_FLAG_TC5);
    DMA1_Channel4->CCR &= ~DMA_CCR4_EN;         //关闭DMA通道4
    DMA1_Channel5->CCR &= ~DMA_CCR5_EN;         //关闭DMA通道5
}

uint8_t spi_send_byte(SPI_TypeDef* SPI, uint8_t byte)
{
    while (SPI_I2S_GetFlagStatus(SPI, SPI_I2S_FLAG_TXE) == RESET);

    SPI_I2S_SendData(SPI, byte);

    while (SPI_I2S_GetFlagStatus(SPI, SPI_I2S_FLAG_RXNE) == RESET);

    return (uint8_t)SPI_I2S_ReceiveData(SPI);
}

uint8_t spi_recv_byte(SPI_TypeDef* SPI, uint8_t byte)
{
    while (SPI_I2S_GetFlagStatus(SPI, SPI_I2S_FLAG_TXE) == RESET);

    SPI_I2S_SendData(SPI, byte);

    while (SPI_I2S_GetFlagStatus(SPI, SPI_I2S_FLAG_RXNE) == RESET);

    return (uint8_t)SPI_I2S_ReceiveData(SPI);
}


int spi_write(SPI_TypeDef* SPI, void *buffer, int size)
{
    char *p=(char *)buffer, ndata;

    while(size--) 
    {
        while((SPI->SR&SPI_I2S_FLAG_TXE)==RESET);
        SPI->DR = *p;
        while((SPI->SR&SPI_I2S_FLAG_RXNE)==RESET);
        ndata = SPI->DR;
        p++;
    }
    return 1;
}

int spi_read(SPI_TypeDef* SPI, void *buffer, int size)
{
    char *p=(char *)buffer;

    while(size--) 
    {
        while((SPI->SR&SPI_I2S_FLAG_TXE)==RESET);
        SPI->DR = 0x00;
        while((SPI->SR&SPI_I2S_FLAG_RXNE)==RESET);
        *p=SPI->DR;
        p++;
    }
    return 1;
}

void spi_high_speed(SPI_TypeDef* SPI)
{
    SPI->CR1&=0XFFC7;
    SPI->CR1|=SPI_BaudRatePrescaler_16;
    SPI_Cmd(SPI,ENABLE);
}

void spi_low_speed(SPI_TypeDef* SPI)
{
    SPI->CR1&=0XFFC7;
    SPI->CR1|=SPI_BaudRatePrescaler_32;
    SPI_Cmd(SPI,ENABLE);
}
