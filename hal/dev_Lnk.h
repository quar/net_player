#ifndef __DEV_LNK_H
#define __DEV_LNK_H


typedef struct _dev_lnk
{
    void *data;
    uint8_t (* _dev_lnk_init)(void *Info);
    uint32_t (* _dev_lnk_write)(void *buffer, uint32_t size);
    uint32_t (* _dev_lnk_read)(void *buffer, uint32_t size);
    uint32_t (* _dev_lnk_ioctl)(uint32_t ioctl, uint32_t io, void *param);
} dev_lnk;

#define dev_lnk_init(lnk, param) ((lnk)->_dev_lnk_init(param))
#define dev_lnk_write(lnk,buffer,size) ((lnk)->_dev_lnk_write(buffer,size))
#define dev_lnk_read(lnk,buffer,size) ((lnk)->_dev_lnk_read(buffer,size))
#define dev_lnk_ioctl(lnk,ioctl,io,param) ((lnk)->_dev_lnk_ioctl(ioctl,io,param))

uint8_t dev_lnk_register(dev_lnk *linker, void *init, void *write, void *read,
    void *ioctl,void *param);


#endif
