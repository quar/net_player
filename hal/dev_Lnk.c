#include "Dev_Lnk.h"

uint8_t dev_lnk_register(dev_lnk *linker, void *init, void *write, void *read,
    void *ioctl,void *param)
{
    linker->_dev_lnk_init = (uint8_t (*)(void *))init;
    linker->_dev_lnk_read = (uint32_t (*)(void *, uint32_t))read;
    linker->_dev_lnk_write = (uint32_t (*)(void *, uint32_t))write;
    linker->_dev_lnk_ioctl = (uint32_t (*)(uint32_t, uint32_t, void *))ioctl;

    return linker->_dev_lnk_init(param);
}
