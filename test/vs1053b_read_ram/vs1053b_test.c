#include "stm32f10x.h"
#include "gpio.h"
#include "config.h"
#include "uart.h"
#include "vs1053B.h"
#include "systick.h"
#include "spi.h"

int main()
{
    uint16_t ret = 0;
    
    vs10xx_init();
    while(1)
    {
        vs10xx_sin_test();
        ret = vs10xx_ram_test();
        printf("%0x\n",ret);
    }
    return 0;
}
