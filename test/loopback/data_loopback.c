#include "w5500_port.h"
#include "uart.h"
#include "config.h"
#include "loopback.h"


#define SOCKET_NO           1
#define PORT                12345
#define _LOOPBACK_DEBUG_
#define IP_ADDR             {192,168,0,140}
uint8_t data_buf[1024] = {0};
int main()
{
    int ret = 0;
    uint8_t dest_ip[4] = IP_ADDR;
    udps_addr_info_t local;
    local.port = 12345;
    local.sn = 1;

    printf("W5500 ping test\r\n");
    ret = w5500_init((udps_addr_info_t*)&local);
    if (ret != 0) {
        printf("w5500 init fail, ret is %d\r\n", ret);
    } else {
        printf("w5500 init success\r\n");
    }
    while(1) {
        //loopback_tcpc(SOCKET_NO, data_buf, dest_ip ,PORT);
        //loopback_tcps(SOCKET_NO, data_buf, PORT);
        loopback_udps(SOCKET_NO, data_buf, PORT);
    }

    return 0;
}