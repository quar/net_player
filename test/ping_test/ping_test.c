#include "w5500_port.h"
#include "uart.h"
#include "config.h"


int main()
{
    int ret = 0;
    Systick_Init(SystemCoreClock/1000000); //1ms

    gpio_init();
    uart_init();
    spi_init(SPI2);

    printf("W5500 ping test\r\n");
    ret = w5500_init(NULL);
    if (ret != 0) {
        printf("w5500 init fail, ret is %d\r\n", ret);
    } else {
        printf("w5500 init success\r\n");
    }
    while(1) {

    }

    return 0;
}


