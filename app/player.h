#ifndef _PLAYER_H_
#define _PLAYER_H_
#include <stdint.h>
#include "config.h"
#ifdef USE_OS
#include "rtthread.h"
#endif

#define PLAYER_THREAD_NAME             "player_thread"
#define PLAYER_THREAD_PRIORITY         4
#define PLAYER_THREAD_STACK_SIZE       1024
#define PLAYER_THREAD_TIMESLICE        5

#define WRITE_BYTES      32

typedef struct _player_contxt
{
    char          *name;
    /* task stack size, set to 0 to use default stack size. unit is 1 byte. */
    uint32_t      stack_size;
    /* task priority */
    uint8_t       task_prio;
    /* task tick */
    uint32_t      tick;
    /* function pointer to entry */
    void          *exe_func;
    /* task handle */
    rt_thread_t   task_handle;
    uint8_t (*data_cbk)(uint8_t **, uint16_t *);
    void (*node_cbk)(uint8_t *);
}player_contxt_t;

void iot_udptxrx_player_register(uint8_t (*d_cbk)(uint8_t **, uint16_t *), void (*n_cbk)(uint8_t *));
int player_init();

#endif
