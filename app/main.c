#include "udp_txrx.h"
#include "player.h"

//static void hook_of_scheduler(struct rt_thread* from, struct rt_thread* to)
//{
//    printf("from: %s -->  to: %s \n", from->name , to->name);
//}

int main()
{
    udptxrx_init();
    player_init();
    iot_udptxrx_player_register(udptxrx_player_data_send_cbk, udptxrx_player_node_send_cbk);

//    rt_scheduler_sethook(hook_of_scheduler);
    
    return 0;
}
