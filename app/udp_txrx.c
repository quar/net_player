#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "udp_txrx.h"
#include "config.h"
#include "uart.h"
#ifdef USE_OS
#include "rtthread.h"
#include "rthw.h"
#endif

static recv_pkt_t         data_pkt[PKT_CNT];
static uint32_t           buf_node[TX_SLID_WINDOWS] = {0};
static udptxrx_contxt_t   udptxrx_contxt;

void queue_init(pktq_t *queue)
{
    queue->list_head = NULL;
    queue->list_tail = NULL;
    queue->depth = 0;
}

/* push in tail/backend */
void pkt_enqueue(pktq_t *queue, list_node_t *p_node)
{
    rt_mutex_take(queue->lock,RT_WAITING_FOREVER);

    if (queue->list_tail == NULL) {
        queue->list_head = queue->list_tail = p_node;
        p_node->next = NULL;
    } else {
        queue->list_tail->next = p_node;
        p_node->next = NULL;
        queue->list_tail = p_node;
    }

    queue->depth++;
    rt_mutex_release(queue->lock);
}

/* pop from head/front */
list_node_t *pkt_dequeue(pktq_t *queue)
{
    list_node_t *entry = NULL;

    rt_mutex_take(queue->lock,RT_WAITING_FOREVER);
    if (queue->list_head) {
        queue->depth--;
        entry = queue->list_head;
        if (queue->list_head == queue->list_tail) {
            queue->list_head = NULL;
            queue->list_tail = NULL;
        } else {
            queue->list_head = queue->list_head->next;
        }
        entry->next = NULL;
    }
    rt_mutex_release(queue->lock);

    return entry;
}

uint8_t queue_empty_check(pktq_t *queue) {

    uint32_t depth;

    rt_mutex_take(queue->lock,RT_WAITING_FOREVER);
    depth = queue->depth;
    rt_mutex_release(queue->lock);

    return (depth > 0) ? ERR_OK : ERR_FAIL;
}

void udptxrx_music_idx_handle(void)
{
    query_pkt_t send_pkt;

    send_pkt.type = QUERY_MUSIC_INDX;
    send_pkt.music.idx = udptxrx_contxt.music_idx;  //query music index
    if (w5500_udps_send(&udptxrx_contxt.dest, (uint8_t *)&send_pkt, sizeof(query_pkt_t))) {
        printf("query music index:%d\r\n", send_pkt.music.idx);
    }
}

void udptxrx_wait_music_idx_handle(void)
{
    uint16_t len;
    recv_idx_pkt_t pkt;

    len = w5500_udps_recv(&udptxrx_contxt.dest, (uint8_t *)&pkt, sizeof(pkt));
    if (len == 0) {
        return;
    }

    if (RESP_MUSIC_MUSIC_INDX == pkt.type) {
        udptxrx_contxt.pkt_cnt = pkt.cnt;
        if (udptxrx_contxt.pkt_cnt) {
            rt_timer_stop(udptxrx_contxt.query_idx_timer);
            rt_timer_start(udptxrx_contxt.inter_timer);
            udptxrx_contxt.pkt_seq = -1;
            udptxrx_contxt.win_min = -1;
            udptxrx_contxt.retry_seq = -1;
            printf("music seq cnt:%d,len:%d\r\n", pkt.cnt,len);
        }
    } else {
        printf("resp type:%d\r\n", pkt.type);
    }
}

void udptxrx_music_data_handle(void)
{
    query_pkt_t send_pkt;

    send_pkt.type = QUERY_MUSIC_DATA;
    if ( udptxrx_contxt.retry_flag == 1) {    //必须要此次要求重传的标志清零，否则正常的seq仍然为重传seq
        udptxrx_contxt.retry_flag = 0;
        udptxrx_contxt.pkt_timer_flag.pkt_timer |= 0x01;
        send_pkt.music.seq = udptxrx_contxt.retry_seq;
    } else {
        udptxrx_contxt.pkt_seq++;
        send_pkt.music.seq = udptxrx_contxt.pkt_seq;
        if (send_pkt.music.seq == (udptxrx_contxt.win_min + TX_SLID_WINDOWS)) {
            udptxrx_contxt.pkt_timer_flag.pkt_timer |= 1<<(TX_SLID_WINDOWS - 1);
        } else {
            udptxrx_contxt.pkt_timer_flag.pkt_timer |= 1<<((udptxrx_contxt.pkt_seq -
                udptxrx_contxt.win_cnt) % TX_SLID_WINDOWS);
        }
    }
    
    if (w5500_udps_send(&udptxrx_contxt.dest, (uint8_t *)&send_pkt, sizeof(query_pkt_t))) {
        printf("send QUERY_MUSIC_DATA,seq = %d\r\n", send_pkt.music.seq);
    }
}

static void move_node(uint32_t *node, uint8_t size)
{
    uint8_t i;
    
    for(i = 0; i < size; i++ ){
        node[i] = node[i+1];
    }
}

void udptxrx_wait_music_data_handle(void)
{
    recv_pkt_t *pkt = NULL;
    uint8_t idx , j = 0;
    static int16_t last_retry_seq = -1;

    if (queue_empty_check(udptxrx_contxt.wq)) {
        printf("no wq\r\n");
        return;
    }

    pkt = (recv_pkt_t*)pkt_dequeue(udptxrx_contxt.wq);
    printf("wq dequeue node addr: %d\r\n", (int)pkt);
    if (pkt == NULL) {
        return;
    }

    pkt->len = 0;
    pkt->len = w5500_udps_recv(&udptxrx_contxt.dest, (uint8_t *)&pkt->recv_data,
        sizeof(pkt->recv_data));
    if ((pkt->len > sizeof(recv_data_pkt_t)) || (pkt->len == 0) ||
        (RESP_MUSIC_MUSIC_DATA != pkt->recv_data.type) ||
        ((RESP_MUSIC_MUSIC_DATA == pkt->recv_data.type) &&
        (pkt->recv_data.seq <= udptxrx_contxt.win_min) &&
        (udptxrx_contxt.win_min != -1))) {
        printf("recv len=%d,type=%d\r\n",pkt->len,pkt->recv_data.type);
        pkt_enqueue(udptxrx_contxt.wq, &pkt->node);
        return;
    }

    //便于打印调试信息
    if ((pkt->recv_data.seq == udptxrx_contxt.retry_seq) &&
        (udptxrx_contxt.retry_seq != -1) && (last_retry_seq != -1)) {
        printf("recv seq=%d,last_retry_seq=%d\r\n", pkt->recv_data.seq, last_retry_seq);
    }

    //seq is from 0 ~ n
    if (pkt->recv_data.seq == udptxrx_contxt.retry_seq) {
        idx = 0;
        last_retry_seq = pkt->recv_data.seq;
    } else if (pkt->recv_data.seq == (udptxrx_contxt.win_min + TX_SLID_WINDOWS)) {
        idx = TX_SLID_WINDOWS - 1;
    } else {
        idx = (pkt->recv_data.seq  - udptxrx_contxt.win_cnt) % TX_SLID_WINDOWS;
    }

    buf_node[idx] = (uint32_t)&pkt->node;
    udptxrx_contxt.pkt_timer_flag.pkt_timer &= ~(1<<idx);
    printf("recv music seq %d:%d B,idx:%d,pkt_timer_flag:%d\r\n", pkt->recv_data.seq,
        pkt->len,idx,udptxrx_contxt.pkt_timer_flag.pkt_timer);
 
    //等待最小序列号数据包收到。就将其加入rq
    while ((udptxrx_contxt.pkt_timer_flag.pkt_timer & 0x01) == 0 && (buf_node[0] != 0) &&
        (j < TX_SLID_WINDOWS) && (udptxrx_contxt.win_min < udptxrx_contxt.pkt_cnt)) {
        udptxrx_contxt.win_cnt = (udptxrx_contxt.win_cnt + 1) % TX_SLID_WINDOWS;
        udptxrx_contxt.win_min = pkt->recv_data.seq + j;
        pkt_enqueue(udptxrx_contxt.rq, (list_node_t *)buf_node[0]);
        printf("rq enqueue node,win_min=%d,buf_node[%d]=%d\r\n", udptxrx_contxt.win_min,
            j, buf_node[0]);
        udptxrx_contxt.pkt_timer_flag.pkt_timer >>= 1;
        move_node(buf_node, 7);
        buf_node[7] = 0;
        j++;
    }
}

void udptxrx_idle_handle(void)
{
    printf("status is idle,idx:%d played,seq %d\r\n", udptxrx_contxt.music_idx, udptxrx_contxt.pkt_seq);
    udptxrx_contxt.music_idx++;
    rt_timer_start(udptxrx_contxt.query_idx_timer);
}

void udptxrx_query_idx_timer_data_path_func(void *parameter)
{
    (void)parameter;

    udptxrx_contxt.tm_cnt++;
    if (udptxrx_contxt.tm_cnt % QUERY_IDX_INVERVAL == 0) {
        if ((udptxrx_contxt.status == WAIT_MUSIC_INDX_STATUS) || (udptxrx_contxt.status == IDLE_STATUS)) {
            udptxrx_contxt.status = QUERY_MUSIC_INDX_STATUS;
            printf("QUERY_MUSIC_INDX_STATUS\r\n");
        }
    } else if (udptxrx_contxt.tm_cnt % WAIT_IDX_INVERVAL == 0) {
        if (udptxrx_contxt.status == QUERY_MUSIC_INDX_STATUS) {
            udptxrx_contxt.status = WAIT_MUSIC_INDX_STATUS;
            printf("WAIT_MUSIC_INDX_STATUS\r\n");
        }
    } else if (udptxrx_contxt.tm_cnt == 1) {
        udptxrx_contxt.status = QUERY_MUSIC_INDX_STATUS;
    } else {
        return;
    }

    rt_mb_send(udptxrx_contxt.mb, (rt_uint32_t)udptxrx_contxt.status);
}

void udptxrx_interval_timer_data_path_func(void *parameter)
{
    uint8_t sta;
    (void)parameter;

    udptxrx_contxt.tm_cnt++;
    if (udptxrx_contxt.tm_cnt % QUERY_DATA_INVERVAL == 0) {
        if (udptxrx_contxt.wq->depth == 0) {
            printf("wq->depth %d,rq->depth %d\r\n", udptxrx_contxt.wq->depth,
            udptxrx_contxt.rq->depth);
            return;
        }

        //若最小序列的窗口未收到序号且请求的窗口满了，不能继续请求，只能等重传
        if ((udptxrx_contxt.pkt_timer_flag.pkt_timer & 0x01) == 1 &&
            (udptxrx_contxt.pkt_seq == (udptxrx_contxt.win_min + TX_SLID_WINDOWS))) {
            rt_timer_control(udptxrx_contxt.retry_timer, RT_TIMER_CTRL_GET_STATE, &sta);
            if (RT_TIMER_FLAG_DEACTIVATED == sta) {
                rt_timer_start(udptxrx_contxt.retry_timer);
            }
            udptxrx_contxt.status = WAIT_MUSIC_MUSIC_DATA_STATUS;
            rt_mb_send(udptxrx_contxt.mb, (rt_uint32_t)udptxrx_contxt.status);
            return;
        }

        if ((udptxrx_contxt.pkt_seq + 1) < udptxrx_contxt.pkt_cnt) {
            udptxrx_contxt.status = QUERY_MUSIC_DATA_STATUS;
            printf("wq->depth %d,rq->depth %d\r\n", udptxrx_contxt.wq->depth,
                udptxrx_contxt.rq->depth);
            rt_mb_send(udptxrx_contxt.mb, (rt_uint32_t)udptxrx_contxt.status);
        } else {
            //避免最后几个序列丢了
            if (udptxrx_contxt.pkt_timer_flag.pkt_timer != 0) {
                rt_timer_control(udptxrx_contxt.retry_timer, RT_TIMER_CTRL_GET_STATE, &sta);
                if (RT_TIMER_FLAG_DEACTIVATED == sta) {
                    rt_timer_start(udptxrx_contxt.retry_timer);
                }
                udptxrx_contxt.status = WAIT_MUSIC_MUSIC_DATA_STATUS;
                rt_mb_send(udptxrx_contxt.mb, (rt_uint32_t)udptxrx_contxt.status);
            } else {
                rt_timer_stop(udptxrx_contxt.inter_timer);
                udptxrx_contxt.status = IDLE_STATUS;
                rt_mb_send(udptxrx_contxt.mb, (rt_uint32_t)udptxrx_contxt.status);
            }
        }
    } else if (udptxrx_contxt.tm_cnt % WAIT_DATA_INVERVAL == 0) {
        udptxrx_contxt.status = WAIT_MUSIC_MUSIC_DATA_STATUS;
        printf("WAIT_MUSIC_MUSIC_DATA_STATUS\r\n");
        rt_mb_send(udptxrx_contxt.mb, (rt_uint32_t)udptxrx_contxt.status);
    }
}

void udptxrx_retry_timer_data_path_func(void *parameter)
{
    uint8_t seq_idx;

    (void)parameter;

    for (seq_idx = 0; seq_idx < TX_SLID_WINDOWS; seq_idx++) {
        if(udptxrx_contxt.pkt_timer_flag.pkt_timer<<(TX_SLID_WINDOWS - seq_idx - 1))
            break;
    }
    if (seq_idx < TX_SLID_WINDOWS) {
        udptxrx_contxt.retry_seq = udptxrx_contxt.win_min + seq_idx + 1;
        udptxrx_contxt.retry_flag = 1;
        printf("retry data query seq %d\r\n", udptxrx_contxt.retry_seq);
        udptxrx_contxt.status = QUERY_MUSIC_DATA_STATUS;
        rt_mb_send(udptxrx_contxt.mb, (rt_uint32_t)udptxrx_contxt.status);
    } else {
        rt_timer_stop(udptxrx_contxt.retry_timer);
        udptxrx_contxt.status = IDLE_STATUS;
    }
}

uint8_t udptxrx_player_data_send_cbk(uint8_t **pdata, uint16_t *len)
{
    uint8_t ret = 0;
    recv_pkt_t *pkt = NULL;
    static int32_t last_seq = -1;

    if (queue_empty_check(udptxrx_contxt.rq)) {
        printf("no rq\r\n");
        return ret;
    }

    pkt = (recv_pkt_t*)pkt_dequeue(udptxrx_contxt.rq);
    if (pkt == NULL) {
        printf("pkt is null\r\n");
        return ret;
    }
    ret = 1;
    *pdata = (uint8_t *)pkt->recv_data.data;
    *len = pkt->len;
    if ((last_seq +1) != pkt->recv_data.seq) {
        printf("seq is not continus\r\n");
    }
    last_seq = pkt->recv_data.seq;
    printf("send player len:%d,seq:%d,node addr:%d,rq->depth %d\r\n", pkt->len,
        pkt->recv_data.seq, (int)&pkt->node, udptxrx_contxt.rq->depth);

    return ret;
}

void udptxrx_player_node_send_cbk(uint8_t *pdata)
{
    list_node_t *node;
    recv_data_pkt_t *data_pkt;
    recv_pkt_t *pkt = NULL;

    if (pdata) {
        data_pkt = container_of(pdata, recv_data_pkt_t, data[0]);
        node = (list_node_t *)container_of(data_pkt, recv_pkt_t, recv_data);
        pkt = (recv_pkt_t *)node;
        printf("player rlease pkt len:%d,seq:%d,node addr:%d\r\n", pkt->len,
            pkt->recv_data.seq, (int)node);
        pkt_enqueue(udptxrx_contxt.wq, node);
   }
}

void udptxrx_entry(void *parameter)
{
    uint8_t status;

    (void)parameter;
    while(1) {
        if (rt_mb_recv(udptxrx_contxt.mb, (rt_uint32_t *)&status, RT_WAITING_FOREVER) == RT_EOK) {
            switch(status) {
            case QUERY_MUSIC_INDX_STATUS:
            {
                udptxrx_music_idx_handle();
                break;
            }
            case WAIT_MUSIC_INDX_STATUS:
            {
                udptxrx_wait_music_idx_handle();
                break;
            }
            case QUERY_MUSIC_DATA_STATUS:
            {
                udptxrx_music_data_handle();
                break;
            }
            case WAIT_MUSIC_MUSIC_DATA_STATUS:
            {
                udptxrx_wait_music_data_handle();
                break;
            }
            case IDLE_STATUS:
            {
                udptxrx_idle_handle();
                break;
            }
            default:
                break;
            }
        }
    }
}

int udptxrx_init()
{
    uint8_t ret = ERR_OK, i;
    uint8_t destip[4] = DEST_IP_ADDR;
    udps_addr_info_t local = {
        .ip = DEFAULT_IP_ADDR,
        .sn = LOCAL_SOCKET_NUM,
        .port = LOCAL_SOCKET_PORT,
    };

    memset(&udptxrx_contxt, 0, sizeof(udptxrx_contxt));

    memset(&data_pkt, 0x0, sizeof(data_pkt));

    w5500_init(&local);   
    udptxrx_contxt.dest.sn = DEST_SOCKET_NUM;
    udptxrx_contxt.dest.port = DEST_SOCKET_PORT;
    memcpy(udptxrx_contxt.dest.ip, destip, sizeof(destip));

    udptxrx_contxt.stack_size = UDPTXRX_THREAD_STACK_SIZE;
    udptxrx_contxt.name = UDPTXRX_THREAD_NAME;
    udptxrx_contxt.task_prio = UDPTXRX_THREAD_PRIORITY;
    udptxrx_contxt.tick = UDPTXRX_THREAD_TIMESLICE;
    udptxrx_contxt.exe_func = udptxrx_entry;
    udptxrx_contxt.task_handle = rt_thread_create(udptxrx_contxt.name, udptxrx_contxt.exe_func,
        NULL, udptxrx_contxt.stack_size, udptxrx_contxt.task_prio, udptxrx_contxt.tick);
    if (udptxrx_contxt.task_handle == NULL) {
        ret = ERR_FAIL;
        goto error_0;
    }
    rt_thread_startup(udptxrx_contxt.task_handle);


    udptxrx_contxt.mb = rt_mb_create("udptxrx mb", 4, RT_IPC_FLAG_FIFO);

    /* init write and read queue and put all will write pkt in queue */
    udptxrx_contxt.wq = rt_malloc(sizeof(pktq_t));
    if (udptxrx_contxt.wq == NULL) {
        ret = ERR_NOMEM;
        goto error_1;
    }

    udptxrx_contxt.rq = rt_malloc(sizeof(pktq_t));
    if (udptxrx_contxt.rq == NULL) {
         ret = ERR_NOMEM;
         goto error_2;
    }
    queue_init(udptxrx_contxt.wq);
    queue_init(udptxrx_contxt.rq);

    udptxrx_contxt.wq->lock = rt_mutex_create("wq_mutex",RT_IPC_FLAG_PRIO);
    if (udptxrx_contxt.wq->lock == NULL) {
        ret = ERR_NOMEM;
        goto error_3;
    }

    udptxrx_contxt.rq->lock = rt_mutex_create("rq_mutex",RT_IPC_FLAG_PRIO);
    if (udptxrx_contxt.rq->lock == NULL) {
        ret = ERR_NOMEM;
        goto error_4;
    }

    for (i = 0; i < PKT_CNT; i++) {
        pkt_enqueue(udptxrx_contxt.wq, &data_pkt[i].node);
        printf("&data_pkt[%d].node=%d\r\n", i, (int)&data_pkt[i].node);
    }

    udptxrx_contxt.query_idx_timer = rt_timer_create("udp txrx query music index timer",
         udptxrx_query_idx_timer_data_path_func, RT_NULL, TIMER_PERIOD,
         RT_TIMER_FLAG_PERIODIC );
    if (udptxrx_contxt.query_idx_timer == RT_NULL) {
         ret = ERR_NOMEM;
         goto error_5;
    }
    rt_timer_start(udptxrx_contxt.query_idx_timer);

    udptxrx_contxt.inter_timer = rt_timer_create("udp txrx interval timer",
        udptxrx_interval_timer_data_path_func, RT_NULL, TIMER_PERIOD,
        RT_TIMER_FLAG_PERIODIC );
    if (udptxrx_contxt.inter_timer == RT_NULL) {
        ret = ERR_NOMEM;
        goto error_6;
    }

    udptxrx_contxt.retry_timer = rt_timer_create("udp txrx retry timer",
        udptxrx_retry_timer_data_path_func, RT_NULL, TIMER_PERIOD,
        RT_TIMER_FLAG_ONE_SHOT );
    if (udptxrx_contxt.retry_timer == RT_NULL) {
        ret = ERR_NOMEM;
        goto error_7;
    }

    printf("creat udptxrx thread successful!\r\n");
    goto success;

error_7:
    rt_timer_delete(udptxrx_contxt.inter_timer);
    printf("creat retry timer failed!\r\n");
error_6:
    rt_timer_delete(udptxrx_contxt.query_idx_timer);
    printf("creat query music index timer failed!\r\n");
error_5:
    rt_mutex_delete(udptxrx_contxt.rq->lock);
    printf("creat interval timer failed!\r\n");
error_4:
    rt_mutex_delete(udptxrx_contxt.wq->lock);
    printf("creat rq lock failed!\r\n");
error_3:
    rt_free(udptxrx_contxt.rq);
    printf("creat wq lock failed!\r\n");
error_2:
    rt_free(udptxrx_contxt.wq);
    printf("malloc rq failed!\r\n");
error_1:
    rt_thread_delete(udptxrx_contxt.task_handle);
    printf("malloc wq failed!\r\n");
error_0:
    printf("creat udptxrx thread failed!\r\n");


success:
    return ret;
}

