#ifndef _UDP_TXRX_H_
#define _UDP_TXRX_H_
#include "w5500_port.h"
#include "config.h"
#ifdef USE_OS
#include "rtthread.h"
#include "rthw.h"
#endif


#define UDPTXRX_THREAD_NAME             "udptxrx_thread"
#define UDPTXRX_THREAD_PRIORITY         3
#define UDPTXRX_THREAD_STACK_SIZE       2048
#define UDPTXRX_THREAD_TIMESLICE        5

#define PKT_CNT                         33
#define TXRX_PKT_LEN                    1024
#define TX_SLID_WINDOWS                 8

#define TIMER_PERIOD                    1   //uint:ms
#define QUERY_IDX_INVERVAL              40 
#define WAIT_IDX_INVERVAL               20
#define QUERY_DATA_INVERVAL             2
#define WAIT_DATA_INVERVAL              1 
#define RETRY_INVERVAL                  3


#define offsetof(TYPE, MEMBER) ((char *)&((TYPE *)0)->MEMBER)
#define container_of(ptr, type, member) ({ \
    const typeof( ((type *)0)->member ) *__mptr = (ptr); \
    (type *)( (char *)__mptr - offsetof(type,member) );})

typedef struct _list_node_t
{
    struct _list_node_t  *next;
} list_node_t;

typedef struct query_pkt
{

    uint32_t type;        //查询的类型
    union
    {
        uint32_t seq;         //数据包的序列号
        uint32_t idx;         //音乐的曲号
    } music;
} query_pkt_t;

typedef struct recv_idx_pkt
{
    uint32_t type;
    uint32_t cnt;
} recv_idx_pkt_t;

typedef struct recv_data_pkt
{
    uint32_t type;
    uint32_t seq;
    uint8_t data[TXRX_PKT_LEN];
} recv_data_pkt_t;

typedef struct recv_pkt
{
    list_node_t node;
    uint16_t len;
    recv_data_pkt_t recv_data;
} recv_pkt_t;

typedef struct _pktq
{
    list_node_t*  list_head;
    /* first in, first out method. add frame in tail. remove from head*/
    list_node_t*  list_tail;
    /* the queue depth */
    uint32_t      depth;
    /* pkt fifo lock */
    rt_mutex_t    lock;
} pktq_t;

typedef struct _udptxrx_contxt
{
    char          *name;
    /* task stack size, set to 0 to use default stack size. unit is 1 byte. */
    uint32_t      stack_size;
    /* task priority */
    uint8_t       task_prio;
    /* task tick */
    uint32_t      tick;
    /* function pointer to entry */
    void          *exe_func;
    /* task handle */
    rt_thread_t   task_handle;
    /* the queue that need to be written */
    pktq_t        *wq;
    /* the queue that need to be read */
    pktq_t        *rq;
    rt_mailbox_t  mb;
    rt_timer_t      query_idx_timer;
    rt_timer_t      inter_timer;
    rt_timer_t      retry_timer;
    udps_addr_info_t dest;
    uint8_t         status;
    uint8_t       music_idx;
    uint16_t      pkt_cnt;
    int16_t      pkt_seq;
    int16_t      retry_seq;
    uint32_t     tm_cnt;
    int16_t      win_min;
    uint16_t      win_cnt;
    uint8_t       retry_flag: 1,
                  reserve:   7;
    union
    {
        uint8_t      pkt_timer;
        struct{
            uint8_t      pkt_timer0:1,
                         pkt_timer1:1,
                         pkt_timer2:1,
                         pkt_timer3:1,
                         pkt_timer4:1,
                         pkt_timer5:1,
                         pkt_timer6:1,
                         pkt_timer7:1;
        };
    } pkt_timer_flag;
}udptxrx_contxt_t;

typedef enum
{
    QUERY_MUSIC_INDX,
    QUERY_MUSIC_DATA,
    RESP_MUSIC_MUSIC_INDX,
    RESP_MUSIC_MUSIC_DATA,
} NETPLAYER_PACKET_TYPE;

typedef enum
{
    QUERY_MUSIC_INDX_STATUS,
    WAIT_MUSIC_INDX_STATUS,
    QUERY_MUSIC_DATA_STATUS,
    WAIT_MUSIC_MUSIC_DATA_STATUS,
    IDLE_STATUS,
} NETPLAYER_PACKET_STATUS;

uint8_t udptxrx_player_data_send_cbk(uint8_t **pdata, uint16_t *len);
void udptxrx_player_node_send_cbk(uint8_t *pdata);
int udptxrx_init();


#endif
