#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "player.h"
#include "vs1053B.h"
#include "systick.h"
#include "uart.h"

static player_contxt_t   player_contxt;

void player_entry(void *parameter)
{
    uint8_t **data = rt_malloc(sizeof(uint8_t *));
    uint8_t *rdata, cnt, pkt_cnt, left ,bytes;
    uint16_t len1 = 0;
    (void)parameter;
    vs10xx_init();
    
    while(1) {
        if(player_contxt.data_cbk((uint8_t **)data, &len1)) {
            rdata = (uint8_t *)*data;
            left = len1 % WRITE_BYTES;
            pkt_cnt = len1/WRITE_BYTES-1;
            for (cnt = 0; cnt <= pkt_cnt; cnt++) {
                bytes = (cnt <= pkt_cnt) ? WRITE_BYTES : left;
                if(vs10xx_write(rdata, bytes) == 1) {
                    printf("vs10xx no write:%d\r\n", cnt);
                    cnt--;
                    Delay_ms(1);
                } else {
                    if (cnt <= pkt_cnt) {
                        rdata = rdata + WRITE_BYTES;
                    }
                }
            }
            player_contxt.node_cbk((uint8_t *)*data);
        }
    }
}

void iot_udptxrx_player_register(uint8_t (*d_cbk)(uint8_t **, uint16_t *), void (*n_cbk)(uint8_t *))
{
    if (d_cbk && n_cbk){
        player_contxt.data_cbk = d_cbk;
        player_contxt.node_cbk = n_cbk;
    }
}

int player_init()
{
    uint8_t ret = ERR_OK, i;

    memset(&player_contxt, 0, sizeof(player_contxt));


    player_contxt.stack_size = PLAYER_THREAD_STACK_SIZE;
    player_contxt.name = PLAYER_THREAD_NAME;
    player_contxt.task_prio = PLAYER_THREAD_PRIORITY;
    player_contxt.tick = PLAYER_THREAD_TIMESLICE;
    player_contxt.exe_func = player_entry;
    player_contxt.task_handle = rt_thread_create(player_contxt.name, player_contxt.exe_func,
        NULL, player_contxt.stack_size, player_contxt.task_prio, player_contxt.tick);
    if (player_contxt.task_handle == NULL) {
        ret = ERR_FAIL;
        goto error_0;
    }
    rt_thread_startup(player_contxt.task_handle);

    printf("creat player thread successful!\r\n");
    goto success;

error_0:
    printf("creat player thread failed!\r\n");


success:
    return ret;
}

