#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "stm32f10x.h"


#define  USE_OS

#define ERR_OK                                  0
/* invalid parameters */
#define ERR_INVAL                               1
/* out of memory */
#define ERR_NOMEM                               2
/* not supported */
#define ERR_NOSUPP                              3
/* not secure due to white list */
#define ERR_NOSEC_WL                            4
/* not exist */
#define ERR_NOT_EXIST                           5
/* again */
#define ERR_AGAIN                               6
/* dev not ready */
#define ERR_NOT_READY                           7
/* already exist */
#define ERR_EXIST                               8
/* busy */
#define ERR_BUSY                                9
/* pending */
#define ERR_PENDING                             10
/* failed */
#define ERR_FAIL                                11
/* not secure due to black list */
#define ERR_NOSEC_BL                            12
/* calculated crc but len < 0 */
#define ERR_CRC_LEN                             13
/* disconnect */
#define ERR_DISCONNECT                          14
/* timeout */
#define ERR_TIMEOVER                            15
/* crc check failed */
#define ERR_CRC_FAIL                            16

#ifdef USE_OS

#endif


#endif
